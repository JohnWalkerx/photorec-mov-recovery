# photorec-mov-recovery

Combines the recovered mov files of PhotoRec to valid video files.

## Why

If you want to recover `*.mov` files from your SD card of your e.g. Canon camera, PhotoRec generates to files: `*_ftyp.mov` and `*_mdat.mov`.

They need to be combined to get a valid video file:
```bash
cat file2_ftyp.mov file1_mdat.mov > video.mov
```

To automate this process this little tool can be used.

## How to use

1. Download [PhotoRec](https://www.cgsecurity.org/wiki/TestDisk_Download)
2. Use as FileOpts:
   ```
   [X] mov/mdat Recover mdat atom as a separate file
   [X] mov      mov/mp4/3gp/3g2/jp2
   ```
3. Recover the files from the SD card with PhotoRec. ([Tutorial](https://www.cgsecurity.org/wiki/PhotoRec_Step_By_Step))
4. Fix permissions:
   ```bash
   chown -R user:group /path/to/photorec/export/recup_dir.4
   ```
5. Download the repository.
6. Change the paths in the `main.go` file to the import and output directory.
7. Make sure in the import directory are only pairs of `_ftyp` and `_mdat` files.
   (At the moment the program doesn't handle this)
8. Run program with `go run .`

**Note:** This is a quick and dirty program. It uses a lot of memory.

## License

The source code is licensed under the [GPL](LICENSE).

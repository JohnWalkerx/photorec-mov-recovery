package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
	"sort"
	"strconv"
	"strings"
)

type MovFile struct {
	FtypFilename, MdatFilename string
}

func sortFilenames(data []string) ([]string, error) {
	var lastErr error
	sort.Slice(data, func(i, j int) bool {
		aSplited := strings.Split(data[i], "_")
		aNumber := aSplited[0][1:]

		a, err := strconv.ParseInt(aNumber, 10, 64)
		if err != nil {
			lastErr = err
			return false
		}

		bSplited := strings.Split(data[j], "_")
		bNumber := bSplited[0][1:]

		b, err := strconv.ParseInt(bNumber, 10, 64)
		if err != nil {
			lastErr = err
			return false
		}
		return a < b
	})
	return data, lastErr
}

func main() {
	// Make sure to have a / at the end of the directory path
	importDirPath := "/path/to/recovered/files/of/photorec/recup_dir.4/"
	exportDirPath := "/path/to/output/directory/"

	log.Println("Start recovery of mov files...")

	files, err := ioutil.ReadDir(importDirPath)
	if err != nil {
		log.Fatal(err)
	}

	var fileNames []string

	// Create list of all matching files
	for _, file := range files {

		if !strings.HasSuffix(file.Name(), ".mov") {
			continue
		}

		fileNames = append(fileNames, file.Name())
	}

	// Sort filenames ascending
	filesSorted, _ := sortFilenames(fileNames)

	var movFiles []MovFile

	// Build array of corresponding mdat and ftyp filenames
	for _, fileName := range filesSorted {
		fileName := fileName

		if !strings.HasSuffix(fileName, ".mov") {
			continue
		}

		if strings.Contains(fileName, "_mdat") {
			newFile := MovFile{MdatFilename: fileName}
			movFiles = append(movFiles, newFile)
		}

		if strings.Contains(fileName, "_ftyp") {
			movFiles[len(movFiles)-1].FtypFilename = fileName
		}
	}

	fileNumber := 1

	// Concat ftyp and mdat file content and write them into output directory
	for _, movFile := range movFiles {

		log.Println("Write file number:", fileNumber, "With ftyp:", movFile.FtypFilename, "mdat:", movFile.MdatFilename)

		// TODO: Use buffers and streaming to avoid high memory load

		contentFtyp, err := ioutil.ReadFile(importDirPath + movFile.FtypFilename)
		if err != nil {
			fmt.Println(err)
		}

		contentMdat, err := ioutil.ReadFile(importDirPath + movFile.MdatFilename)
		if err != nil {
			fmt.Println(err)
		}

		resultFile := bytes.Join([][]byte{contentFtyp, contentMdat}, []byte{})

		ioutil.WriteFile(exportDirPath+"vid"+strconv.FormatInt(int64(fileNumber), 10)+".mov", resultFile, 0644)

		fileNumber++
	}

	log.Println("Converting finished!")
}
